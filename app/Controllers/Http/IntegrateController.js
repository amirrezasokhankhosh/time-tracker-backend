'use strict'

const axios = require('axios')

class IntegrateController {

    async index({ request, response }) {
        // pk_6657775_FEE14WW33K5AXS4FPRXYHCDFRTDVTYB6
        // team_id : 2668575 // amirreza's workspace
        // space_id : 4756222 // Test space
        // folder_id : 15216898 // Test Folder
        // list_id : 33952355 // Test List 
        axios
            .post(`https://api.clickup.com/api/v2/list/33952355/task/`, {
                "name": "Check Task2",
                "description": "New Task Description",
                
            }, {
                headers: {
                    Authorization: `pk_6657775_FEE14WW33K5AXS4FPRXYHCDFRTDVTYB6`
                },
            }).then((response) => {
                console.log(response.data)
            })

    }
}

module.exports = IntegrateController
